﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyControllerAnimator : MonoBehaviour{

    Animator animController;
    
    void Start(){
        animController = GetComponent<Animator>();
    }

    void Update(){
        //левая кнопка мыши
        if (Input.GetMouseButtonDown(0)){
            animController.SetBool("idleBool", true);
        }

        //ПКМ
        if (Input.GetMouseButtonDown(1)){
            animController.SetBool("idleBool", false);
        }    

        if (Input.GetKeyDown(KeyCode.Space)){
            animController.SetInteger("life", 0);
        }
    }
}
