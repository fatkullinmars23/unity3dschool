﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour{
    
    public bool isSameDelay;
    public bool isRandomObject;
    public GameObject ObjectToSpawn;
    public GameObject[] ObjectsToSpawn;
    public float timeBeforSpawn;
    public float spawnDelay;
    public float MinDelay;
    public float MaxDelay;
    void Start(){
        if (isSameDelay) {
            InvokeRepeating("Spawn", timeBeforSpawn, spawnDelay);
        } else {
            StartCoroutine("Spawner");        
        }
    }

    //Рандомный спаун объектов в промежутке 2 и 6 (числа любые)
    IEnumerable Spawner() {
        yield return new WaitForSeconds(Random.Range(MinDelay, MaxDelay));
        Spawner();
    }

    void Spawn(){
        if (!isRandomObject){
            GameObject obj = Instantiate(ObjectToSpawn, transform.position, transform.rotation) as GameObject;
        }
        if (isRandomObject){
            GameObject obj = Instantiate(ObjectsToSpawn[Random.Range(0, ObjectsToSpawn.Length)], transform.position, transform.rotation) as GameObject; 
        }
        if (!isSameDelay){
            StartCoroutine("Spawner");  
        }
    }
    
    void Update(){
        
    }
}
