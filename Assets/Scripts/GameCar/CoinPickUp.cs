﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CoinPickUp : MonoBehaviour
{
    public Text coinCount;   
    public string filename; 
    void Start(){
        coinCount = GameObject.Find("CoinCount").GetComponent<Text>();
        if (filename == "") filename = "Assets/Data/Save/Score.sg";
    }

    public string RetCoinCount(){
        coinCount.text = (int.Parse(coinCount.text)+1).ToString();  
        //My_text.MyText = coinCount.text; //Вывод в GameOver
        WriteToFileScore(coinCount.text);
        return coinCount.text;
    }

    void OnTriggerEnter(Collider coinCollider){
        if (coinCollider.name == "CarHead"){
            RetCoinCount();
            Destroy(gameObject);
        }
    }

    void WriteToFileScore(string score){
        StreamWriter sw = new StreamWriter(filename);
        sw.WriteLine(score);
        Debug.Log("Score: " + score);
        sw.Close();
    }
}
