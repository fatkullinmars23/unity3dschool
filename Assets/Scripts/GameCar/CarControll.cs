﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarControll : MonoBehaviour{
    
    public Transform target;
    public Vector3 moveLeftVector;
    public Vector3 moveRightVector;
    public float speed;
	public bool isLeft;
	public bool isRight;
	
    void Start(){
        
    }

    void Update(){
        Vector3 movePos = new Vector3(target.position.x, 0.34f, 11.53f);
/*        if (Input.GetKey(KeyCode.A)){
            if (target.transform.position.x < 3.3f){
                target.transform.Translate(moveRightVector * Time.deltaTime * speed);
            }
        }

        if (Input.GetKey(KeyCode.D)){
            if (target.transform.position.x > -3.3f){
                target.transform.Translate(moveLeftVector * Time.deltaTime * speed);
            }    
        }
*/
		if (isLeft) {
			if (target.transform.position.x < 3.3f){
				target.transform.Translate(moveLeftVector * Time.deltaTime * speed);
			}
		}
		if (isRight) {
			if (target.transform.position.x > -3.3f){
				target.transform.Translate(moveRightVector * Time.deltaTime * speed);
			} 
		}
        transform.LookAt(target);
        transform.position = Vector3.MoveTowards(transform.position, movePos, Time.deltaTime * (speed-0.5f));
    }
	
	public void TurnLeft(bool isOn) {
		isLeft = isOn;
	}

    public void TurnRight(bool isOn)
    {
		 isRight = isOn;
	}
}
