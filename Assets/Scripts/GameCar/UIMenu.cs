﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class UIMenu : MonoBehaviour
{
    public Text scoreText;  
    string fileName = "Assets/Data/Save/Score.sg";
    private void Start(){
        StreamReader sr = new StreamReader(fileName);
        if (sr != null) {
            while (!sr.EndOfStream){
                scoreText.text = System.Convert.ToString(sr.ReadLine());
            }
        }        
    }
}
