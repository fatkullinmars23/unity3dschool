﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGameObject : MonoBehaviour{

    public Vector3 moveVector;
    public float speed;
    public float TimeBeforeDestroy;

    void Start(){
        Destroy(gameObject, TimeBeforeDestroy);
    }
    
    void Update(){
        transform.Translate(moveVector * Time.deltaTime * speed);
    }
}
